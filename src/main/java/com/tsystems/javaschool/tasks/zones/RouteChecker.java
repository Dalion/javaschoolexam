package com.tsystems.javaschool.tasks.zones;

import java.util.List;

public class RouteChecker {
    
    
    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    
    //adjacency matrix
    private int[][] matrix;
    
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds) {
        for (Integer zoneId: requestedZoneIds) {
            //checks, whether zoneState contains all requested zones
            if (getZone(zoneState, zoneId)==null) {
                return false;
            }
            //checks neighbours of requested zones
            try {
                getZone(zoneState, zoneId).getNeighbours();
            } catch (NullPointerException e) {
                return false;
            }
        }
        matrix = new int[zoneState.size()][zoneState.size()];
        int matrixLine=0;
        for (Zone zone: zoneState) {
            //checks, if zone has ID equal any ID of requested zone, then fill line of adjacency matrix
            if (requestedZoneIds.contains(zone.getId())) {
                writeNeighbours(zoneState, requestedZoneIds, matrixLine, zone.getId());
            }
            matrixLine++;
        }
        makeBidirectional();
        return checkIfRequestedZonesAreConnected(requestedZoneIds.size());
    }
    
    //writes all vertexes we could reach from current vertex, represented by a line index of adjacency matrix
    private void writeNeighbours (List<Zone> zoneState, List<Integer> requestedZoneIds, int matrixLine, int zoneId) {
        for (Integer neighbourId : getZone(zoneState, zoneId).getNeighbours()) {
            /*
             * indexOf() used to prevent situations, when zone ID is random
             * and have no connection with index of element in the zone list.
             * actually we get index of neighbourID
             */
            int indexOfNeighbourId = zoneState.indexOf(getZone(zoneState,neighbourId));
            if (requestedZoneIds.contains(neighbourId) && indexOfNeighbourId!=matrixLine) {
                
                //We check, if we were here already and it seems to become infinite loop
                if (matrix[matrixLine][indexOfNeighbourId] != 1) {
                    matrix[matrixLine][indexOfNeighbourId] = 1;
                }
                else {
                    break;
                }
                
                //checks, whether current neighbour vertex has another neighbours, cause we have to reach leave
                if (!getZone(zoneState, neighbourId).getNeighbours().isEmpty()) {
                    writeNeighbours(zoneState, requestedZoneIds, matrixLine, neighbourId);
                }
            }
        }
        return;
    }
    
    //changes adjacency matrix, according to condition of bidirectional connections
    private void makeBidirectional () {
        for (int row=0; row<matrix.length;row++) {
            boolean flag=true;
            int buf=0;
            for (int line=0; line<matrix.length;line++) {
                if (matrix[line][row]!=0) {
                    if (flag) {
                        flag=false;
                        buf=line;
                    }
                    else {
                        matrix[line][buf]=1;
                    }
                }
            }
        }
        return;
    }
    
    //returns zone, found by zone ID
    private Zone getZone (List<Zone> zoneState, int zoneId) {
        for (Zone zone: zoneState) {
            if (zone.getId()==zoneId) {
                return zone;
            }
        }
        return null;
    }
    
    /*
     * as result, we have adjacency matrix filled only with connections of requested zones
     * other elements are zeros. So we can count number of non-zero elements in each line
     * and number of this elements represents number of vertexes we can reach from one vertex,
     * ID of this vertex isn't important.
     * If counted number equal number of vertexes, requested zones are connected.
     */
    private boolean checkIfRequestedZonesAreConnected (int vertexesNumber) {
        for (int line=0, count=1; line<matrix.length; line++) {
            for (int row=0; row<matrix.length; row++) {
                if (matrix[line][row]!=0) {
                    count++;
                }
            }
            if (count==vertexesNumber){
                return true;
            }
            else {
                count=1;
            }
        }
        return false;
    }
}
